-----------------------------------------------------------------------------------------
---Title : README.txt                                                                   -
---Version : 1.0                                                                        -
---CreatedDate : 2020-11-07 18:15:00                                                    -
---CreatedBy : AntoineB                                                                 -
-----------------------------------------------------------------------------------------

Table of Contents
1.General Info
2.Prerequisite
3.Scripts
4.Configure Scripts
5.Use Scripts

-----------------------------------------------------------------------------------------
---------------------------------- General Info -----------------------------------------
-----------------------------------------------------------------------------------------
The program find the frequency of occurence of the word in a list compared to a another list.

-----------------------------------------------------------------------------------------
---------------------------------- Prerequisite -----------------------------------------
-----------------------------------------------------------------------------------------
Python must be installed with a minimum version v3.*.
You can install it on : https://www.python.org/

Docker must be installed : https://www.docker.com/

The lists to test and compare must not exceed 1000 values.

-----------------------------------------------------------------------------------------
------------------------------------- Scripts -------------------------------------------
-----------------------------------------------------------------------------------------
1.SparseArray.py
This file defined the function to count occurrences of word appearances.

2.main.py
This file must be used to launch the program .
This main file etablish the result occurence.
It relies on file "SparseArray.py".

3.Dockerfile
This file is a text document that contains all the commands a user could call on the command line to assemble an image.

-----------------------------------------------------------------------------------------
-------------------------------- Configure Scripts --------------------------------------
-----------------------------------------------------------------------------------------
1.Dockerfile
You could change the reference values in the Dockerfiles : modify the parameter PY_STRINGS.

-----------------------------------------------------------------------------------------
----------------------------------- Use Scripts -----------------------------------------
-----------------------------------------------------------------------------------------
1.Open the cmd windows
2.Construct the Docker image with the command : docker build . -t test_mdm
3.Run the container with the command : docker run -t test_mdm [list of values]
[list of values] :  list of values to test and have the the frequency of occurence. Values must separate by a coma
Example : docker run -t test_mdm a,b,aa,fg
Result : {'a': '1', 'b': '0', 'aa': '0', 'fg': '0'}

You can write many list, separate by a space.
Exemple : docker run -t test_mdm a,b,aa,fg g,toto,tata
Result : {'a': '1', 'b': '0', 'aa': '0', 'fg': '0'}
		 {'g': '0', 'toto': '1', 'tata': '1'}
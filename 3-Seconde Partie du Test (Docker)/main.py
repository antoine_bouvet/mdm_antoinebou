############################################################################################
#Title : main.py                                                                           #
#Version : 1.0                                                                             #
#CreatedDate : 2020-11-06 09:00:00                                                         #
#CreatedBy : AntoineB                                                                      #
#															      					       #
#Description : This main file etablish the result occurence                                #
#It relies on file "SparseArray.py"                                                        #
############################################################################################

#!/bin/python3

import os

from SparseArray import *

if __name__ == '__main__':

    #Creation and supply of the environment variable.
    #os.environ['PY_STRINGS'] = 'a,df,ee,dft,skt,toto,tata,titi,pp,ds,ps,xn'
    Strings = os.environ['PY_STRINGS'].split(',')

    #The test if at least one argument has been passed in the command.
    #If no arguments --> program stop with instructions.
    if len(sys.argv[1:])==0:
        print('There are no arguments. Please to add at least one list.')
        exit()
	
	#Browse the arguments
    for arg in sys.argv[1:]:

        #The queries list must not exceed 1000 values.
        #If the list exceeds 1000 values --> program stop with instructions.
        if len(arg.split(',')) > 1000:
            print('The maximum number of arguments is 1000. Please modify the list "' + arg + '".')
            exit()
        
        #The test if the list Strings is not empty
		#If the list is empty --> program stop with instructions.
        if Strings=='':
            print('There is no value in the environment variable "Strings"')
            exit()
			
        #The queries list must not exceed 1000 values.
        #If the list exceeds 1000 values --> program stop with instructions.
        if len(Strings) > 1000:
            print('The maximum number of arguments is 1000. Please modify the environment variable "Strings".')
            exit()
            
        Queries = arg.split(",")

		#Use the function "Matching_Strings" in the file "SparseArray.py"
        Result = Matching_Strings(Strings, Queries)
        
		#Show the result
        print(Result)
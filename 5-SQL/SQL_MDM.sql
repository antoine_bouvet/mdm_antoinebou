-----------------------------------------------------------------------------------------
---Title : SQL_MDM.sql                                                                  -
---Version : 1.0                                                                        -
---CreatedDate : 2020-11-07 18:10:00                                                    -
---CreatedBy : AntoineB                                                                 -
-----------------------------------------------------------------------------------------


----- Calcul du Chiffres d'affaires quotidiens pour l'année 2019 -----
SELECT 
	date as Dates
	, SUM(prod_price * prod_qty) as CA 
FROM TRANSACTIONS
WHERE date between '01/01/2019' and '31/12/2019'
GROUP BY date
ORDER BY date


----- Calcul des ventes Meuble et Déco par client pour l'année 2019 -----
SELECT 
	TRANSACTIONS.cliend_id as Identifiant_Client
	, SUM(CASE WHEN PRODUCT_NOMENCLATURE.product_typ = 'MEUBLE' THEN TRANSACTIONS.prod_price * TRANSACTIONS.prod_qty ELSE 0 END) as Ventes_Meuble
	, SUM(CASE WHEN PRODUCT_NOMENCLATURE.product_typ = 'DECO' THEN TRANSACTIONS.prod_price * TRANSACTIONS.prod_qty ELSE 0 END) as Ventes_Deco
FROM TRANSACTIONS
	INNER JOIN PRODUCT_NOMENCLATURE ON PRODUCT_NOMENCLATURE.product_id = TRANSACTIONS.prod_id
WHERE TRANSACTIONS.date between '01/01/2019' and '31/12/2019'
GROUP BY TRANSACTIONS.cliend_id
ORDER BY TRANSACTIONS.cliend_id
#!/bin/python3

import math
import os
import random
import re
import sys
from collections import Counter

# Complete the matchingStrings function below.
def matchingStrings(strings, queries):
    a = ''
    lg_strings = len(strings)
    lg_queries = len(queries)

    #parcours de la liste 'queries' et comptage des occurences dans la liste 'strings'
    for j in range(lg_queries):
        #a = a + ' ' + queries[j] + ': ' + str(strings.count(queries[j])) + '\n'
        a = a + str(strings.count(queries[j])) + '\n'
    return a

#Lorsque vous exécutez votre script, la __name__variable est égale à __main__. Lorsque vous importez le script contenant, il contiendra le nom du script.
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    #fptr = open(os.environ["C:/Users/abouvet/Desktop/Test.txt"], 'w')
    #fptr = open("C:/Users/abouvet/Desktop/Test.txt", 'w')

    strings_count = int(input())

    strings = []

    for _ in range(strings_count):
        strings_item = input()
        strings.append(strings_item)

    queries_count = int(input())

    queries = []

    for _ in range(queries_count):
        queries_item = input()
        queries.append(queries_item)

    res = matchingStrings(strings, queries)
    
    #fptr.write('\n'.join(map(int, res)))
    fptr.write(res)
    fptr.write('\n')

    fptr.close()

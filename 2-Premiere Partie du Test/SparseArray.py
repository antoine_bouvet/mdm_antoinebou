#######################################################################################
#Title : SparseArray.py                                                               #
#Version : 1.0                                                                        #
#CreatedDate : 2020-11-06 08:43:00                                                    #
#CreatedBy : AntoineB                                                                 #
#															      					  #
#Description : This file defined the function to count occurrences of word appearances#
#######################################################################################

#!/bin/python3

import os
import sys

#Creation of the function 'matchingStrings' with 2 parameters : "strings" (an array of strings to search) and "queries" (an array of query strings)
#This function returns a dictionary with the frequency of occurence of queries words in strings
def Matching_Strings(Strings, Queries):

    Result = dict()
    Length_Queries = len(Queries)

	#Browse the list "Queries"
    for j in range(Length_Queries):
        Result[Queries[j]] = str(Strings.count(Queries[j]))
		
    return Result


